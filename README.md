# Stonkz Backend
A backend Java Springboot application that allows users to add and remove stocks
as well as make trades and view prices

## Functionality
 - For a given stock,
	 - Given any price as input, calculate the dividend yield
	 - Given any price as input, calculate the P/E Ratio
	 - Record a trade, with timestamp, quantity of shares, buy or sell indicators and traded price
	 - Calculate Volume Weighted Stock Price based on trades in the past 15 minutes
- Calculate the GBCE All Share Index using the geometric mean of prices for all stocks

## Next steps
- add more comprehensive API suite to interact with the services and data
- add database to persist the stock and trade data

## Constraints & Notes
- No database or GUI provided
	- All data is held in memory at runtime although a mongodb is being implemented
- 91% Class and Method test coverage
